import './App.css';
import 'antd/dist/antd.css';
import Projects from './components/Projects';

function App() {
  return (
    <div className="App">
      <Projects />
    </div>
  );
}

export default App;
