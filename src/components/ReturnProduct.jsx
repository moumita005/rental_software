

import React from "react";
import { Select, DatePicker, Button, Modal, Input } from "antd";
import { CalendarTwoTone } from '@ant-design/icons';
import moment from "moment";
import BookProductPrice from "./BookProductPrice";
import { useState } from "react";
import ReturnProductPrice from "./ReturnProductPrice";

const { Option } = Select;

export default function ReturnProduct({
  data,
  handleSubmit,
  projects,
  show,
  handleChange,
  handleDataChange,
  handleClose,
  handleShow,
  handleInvisible
}) {
  const [producData, setProducData] = useState({});
  const [views, setViews] = useState({});
  const handleView = ({type, payload}) => {
    setProducData(payload);
    setViews({...views, [type]:true})
  }
  const handleViewClose = () => {
      setViews({});
      setProducData({})
  }
  

  return (
    <div>
      <div className="container py-3">
        <p>Select Product</p>
        <Select
          showSearch
          style={{ width: "100%" }}
          placeholder="Select a Product"
          optionFilterProp="children"
          onChange={(value) => handleDataChange({ name: "product", value })}
          name="product"
          value={data.product}
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {projects
            .filter((item) => item.availability === false)
            .map((item, index) => (
              <Option key={index} value={item.code}>
                {item.name}
              </Option>
            ))}
        </Select>
        <p className="pt-3">Used Mileage</p>
        <Input
           value={data.mileage}
        />
        <div className="mb-3 mt-5 text-right">
          <Button onClick={handleClose} type="link">
            No
          </Button>
          <Button
            onClick={() => {
                handleView({ type: "price", payload: data });
            }}
            type="link"
          >
            Yes
          </Button>
        </div>
        <Modal
          title="Book a Product"
          width={window.innerWidth > 900 ? 800 : window.innerWidth - 50}
          onCancel={handleViewClose}
          visible={views.price}
          bodyStyle={{ paddingBottom: 80 }}
          footer={null}
          centered
        >
          <ReturnProductPrice
            projects={projects}
            data={producData}
            handleSubmit={handleSubmit}
            handleClose={handleViewClose}
          />
        </Modal>
      </div>
    </div>
  );
}
