import React, { Component } from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Button, Modal } from "antd";
import data from "../data/data.json";
import { useState } from "react";
import { useEffect } from "react";
import BookProduct from "./BookProduct";
import ReturnProduct from "./ReturnProduct";
import moment from "moment";

export default function Projects() {
  const [projects, setProjects] = useState([]);
  const [search, setSearch] = useState("");
  const [show, setShow] = useState({});
  const [projectData, setProjectData] = useState({});
  const [errors, seterrors] = useState({});
  const [producData, setProducData] = useState({});
  const [views, setViews] = useState({});

  useEffect(() => {
    (() => {
      setProjects(data);
    })();
  }, [projects]);

  const handleChange = (e) => {
    setSearch(e.target.value);
    setProjectData({ ...projectData, [e.target.name]: e.target.value });
  };
  const handleDataChange = (input) => {
    const newData = { ...projectData, [input.name]: input.value };
    if (input.name === "product") {
      const project = projects.find((project) => project.code === input.value);
      let bookingDate = localStorage.getItem("fromDate");
      let date1 = new Date();
      let date2 = new Date(bookingDate);
      let diffDays = parseInt((date1 - date2) / (1000 * 60 * 60 * 24), 10);
      newData.mileage = Number(project.mileage) + 10 * diffDays;
      newData.durability =
        project.type === "plain"
          ? project.max_durability - 1 * diffDays
          : project.type === "meter"
          ? project.max_durability - 2 * diffDays
          : project.max_durability;
      console.log(newData.durability);
    }
    setProjectData(newData);
  };

  const handleShow = ({ type, payload }) => {
    console.log("data", payload);
    setProjectData(payload);
    setShow({ ...show, [type]: true });
  };

  const handleClose = () => {
    console.log("i am here");
    setProjectData({});
    setShow({});
  };

  const handleView = ({ type, payload }) => {
    setProducData(payload);
    setViews({ ...views, [type]: true });
  };
  const handleViewClose = () => {
    setViews({});
    setProducData({});
  };

  const handleSubmit = () => {
    const { product, from_date, to_date, product: returnProduct } = projectData;
    localStorage.setItem("product", product);
    localStorage.setItem("fromDate", from_date);
    localStorage.setItem("toDate", to_date);
    console.log("prjtData", projectData, returnProduct);
    // setProjectData({})
    // setShow({})
  };

  return (
    <div>
      <div className="card  ">
        <div className="card-header">
          <span className="na-text" style={{ fontSize: "30px" }}>
            projects
          </span>
        </div>
        <div className="">
          <div className="row pr-3">
            <div className="col-md-8"></div>
            <div className="col-md-4 ">
              <input
                name="search"
                value={search}
                type="text"
                className="form-control my-4 mr-5"
                placeholder="Search......"
                onChange={handleChange}
              />
            </div>
          </div>
          <TableContainer component={Paper}>
            <Table aria-label="simple table" size="small">
              <TableHead style={{ background: "#4288bf " }}>
                <TableRow>
                  <TableCell
                    className="font-weight-bold text-white"
                    align="left"
                  >
                    ID
                  </TableCell>
                  <TableCell
                    className="font-weight-bold text-white"
                    align="left"
                  >
                    Name
                  </TableCell>
                  <TableCell
                    className="font-weight-bold text-white"
                    align="left"
                  >
                    Code
                  </TableCell>
                  <TableCell
                    className="font-weight-bold text-white"
                    align="left"
                  >
                    Availability
                  </TableCell>
                  <TableCell
                    className="font-weight-bold text-white"
                    align="left"
                  >
                    Need to Repair
                  </TableCell>
                  <TableCell
                    className="font-weight-bold text-white"
                    align="left"
                  >
                    Durability
                  </TableCell>

                  <TableCell
                    className="font-weight-bold text-white"
                    align="left"
                  >
                    Mileage
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {projects
                  .filter((val) => {
                    if (search == "") {
                      return val;
                    } else if (
                      val.name.toLowerCase().includes(search.toLowerCase()) || 
                      val.code.toLowerCase().includes(search.toLowerCase()) 
                    ) {
                      return val;
                    }
                  })
                  .slice(0, 17)
                  .map((project, index) => {
                    return (
                      <TableRow key={index} className="tr">
                        <TableCell>{index + 1} </TableCell>
                        <TableCell>{project.name} </TableCell>
                        <TableCell align="left">{project.code}</TableCell>
                        <TableCell align="left">
                          {project.availability === true ? "True" : "False"}
                        </TableCell>
                        <TableCell align="left">
                          {project.needing_repair === true ? "True" : "False"}
                        </TableCell>
                        <TableCell align="left">{project.durability}</TableCell>
                        <TableCell align="left">{project.mileage}</TableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
          <div className="py-3">
            <Button
              type="primary"
              className="mr-4"
              onClick={() => handleShow({ type: "return", payload: {} })}
            >
              Return Product
            </Button>
            <Button
              type="primary"
              onClick={() => handleShow({ type: "view", payload: {} })}
            >
              Book Product
            </Button>
          </div>

          <Modal
            title="Book a Product"
            width={window.innerWidth > 900 ? 800 : window.innerWidth - 50}
            onCancel={handleClose}
            visible={show.view}
            bodyStyle={{ paddingBottom: 80 }}
            footer={null}
            closable
            centered
          >
            <BookProduct
              projects={projects}
              data={projectData}
              show={show}
              handleChange={handleChange}
              handleDataChange={handleDataChange}
              handleShow={handleShow}
              handleSubmit={handleSubmit}
              handleClose={handleClose}
              handleView={handleView}
              handleViewClose={handleViewClose}
            />
          </Modal>
          <Modal
            title="Return a Product"
            width={window.innerWidth > 900 ? 800 : window.innerWidth - 50}
            onCancel={handleClose}
            visible={show.return}
            bodyStyle={{ paddingBottom: 80 }}
            footer={null}
            closable
            centered
          >
            <ReturnProduct
              projects={projects}
              data={projectData}
              show={show}
              handleChange={handleChange}
              handleDataChange={handleDataChange}
              handleShow={handleShow}
              handleSubmit={handleSubmit}
              handleClose={handleClose}
              handleView={handleView}
              handleViewClose={handleViewClose}
            />
          </Modal>
        </div>
      </div>
    </div>
  );
}
