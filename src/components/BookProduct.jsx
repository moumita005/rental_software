import React from "react";
import { Select, DatePicker, Button, Modal, Input } from "antd";
import { CalendarTwoTone } from '@ant-design/icons';
import moment from "moment";
import BookProductPrice from "./BookProductPrice";
import { useState } from "react";

const { Option } = Select;

export default function BookProduct({
  data,
  handleSubmit,
  projects,
  show,
  handleChange,
  handleDataChange,
  handleClose,
  handleShow,
  handleInvisible
}) {
  const [producData, setProducData] = useState({});
  const [views, setViews] = useState({});
  const handleView = ({type, payload}) => {
    setProducData(payload);
    setViews({...views, [type]:true})
  }
  const handleViewClose = () => {
      setViews({});
      setProducData({})
  }
  
  return (
    <div>
      <div className="container py-3">
        <p>Select Product</p>
        <Select
          showSearch
          style={{ width: "100%" }}
          placeholder="Select a Product"
          optionFilterProp="children"
          onChange={(value) => handleDataChange({ name: "product", value })}
          name="product"
          value={data.product}
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {projects
            .filter((item) => item.availability === true && item.needing_repair === false)
            .map((item, index) => (
              <Option key={index} value={item.code}>
                {item.name}
              </Option>
            ))}
        </Select>
        <div className="row my-3">
          <div className="col-md-1">From</div>
          <div className="col-md-5">
            <DatePicker
              placeholder="From Date"
              style={{ width: "100%" }}
              name="from_date"
              value={data.from_date && moment(data.from_date)}
              onChange={(value) =>
                handleDataChange({ name: "from_date", value })
              }
              format="DD/MM/YYYY"
              suffixIcon={
                <CalendarTwoTone
                  twoToneColor="#bfbfbf"
                  style={{ fontSize: '25px' }}
                />
              }
            />
          </div>
          <div className="col-md-1">To</div>
          <div className="col-md-5">
            <DatePicker
              placeholder="To Date"
              style={{ width: "100%" }}
              name="to_date"
              value={data.to_date && moment(data.to_date)}

              onChange={(value) => handleDataChange({ name: "to_date", value })}
              format="DD/MM/YYYY"
              suffixIcon={
                <CalendarTwoTone
                  twoToneColor="#bfbfbf"
                  style={{ fontSize: '25px' }}
                />
              }
            />
          </div>
        </div>
        <div className="mb-3 mt-5 text-right">
          <Button onClick={handleClose} type="link">
            No
          </Button>
          <Button
            onClick={() => {
                handleView({ type: "price", payload: data });
            }}
            type="link"
          >
            Yes
          </Button>
        </div>
        <Modal
          title="Book a Product"
          width={window.innerWidth > 900 ? 800 : window.innerWidth - 50}
          onCancel={handleViewClose}
          visible={views.price}
          bodyStyle={{ paddingBottom: 80 }}
          footer={null}
          centered
        >
          <BookProductPrice
            projects={projects}
            data={producData}
            handleSubmit={handleSubmit}
            handleClose={handleViewClose}
          />
        </Modal>
      </div>
    </div>
  );
}
