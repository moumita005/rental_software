import moment from "moment";
import React from "react";
import {Button} from 'antd'

export default function BookProductPrice({
  data,
  handleSubmit,
  projects,
  show,
  handleChange,
  handleDataChange,
  handleClose,
  handleShow,
}) {
  const find = projects.find((item) => item.code === data.product);
  let a = moment(data.from_date), b = moment(data.to_date,).add(1,'days')
  let c = b.diff(a, 'days')
//   let differ = a.diff(b, "days"); 
  
  console.log("price data", data, "find", find,c);
  const totalPrice = Number(find.minimum_rent_period)>c? Number(find.price)* Number(find.minimum_rent_period) : Number(find.price)*c
  return( 
  <div>
      Your estimated price is {totalPrice}
      <div>Do you want to <span className="text-primary">Procedure</span>?</div>
      <div className="mb-3 mt-5 text-right">
          <Button onClick={handleClose} type="link">
            No
          </Button>
          <Button
            onClick={handleSubmit}
            type="link"
          >
            Yes
          </Button>
        </div>
  </div>

    );
}
