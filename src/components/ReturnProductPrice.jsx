import moment from "moment";
import React from "react";
import {Button} from 'antd'

export default function ReturnProductPrice({
  data,
  handleSubmit,
  projects,
  show,
  handleChange,
  handleDataChange,
  handleClose,
  handleShow,
}) {
  const find = projects.find((item) => item.code === data.product);
  let bookingDate = localStorage.getItem("fromDate");
  var date1 = new Date();
  var date2 = new Date(bookingDate);
  var c = parseInt((date1 - date2) / (1000 * 60 * 60 * 24), 10); 
  
  console.log("price data", data, "find", find,date1,date2,c);
  const totalPrice = Number(find.minimum_rent_period)>c? Number(find.price)* Number(find.minimum_rent_period) : Number(find.price)*c
  return( 
  <div>
      Your total price is {totalPrice}
      <div>Do you want to <span className="text-primary">Procedure</span>?</div>
      <div className="mb-3 mt-5 text-right">
          <Button onClick={handleClose} type="link">
            No
          </Button>
          <Button
            onClick={handleSubmit}
            type="link"
          >
            Yes
          </Button>
        </div>
  </div>

    );
}
